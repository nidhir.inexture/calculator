import React,{useState} from 'react';
import './App.css';



function App() {
  const [result, setResult] = useState("");

  const clickHandler = (e) => {
    setResult(result.concat(e.target.value));
  }

  const clearDisplay = () => {
    setResult("");
  }
  
  const calculate = () => {
    setResult(eval(result));
  }
    return (
      <div className="App">
       <div className='title'>
        <h1>Calculator App</h1>
       </div>
       <div className='main'>
       <form>
         <input type="text" id="output" readOnly value={result} className="input"/><br/>
         <input type="button" value="9" onClick={clickHandler} className="button"/>
         <input type="button" value="8" onClick={clickHandler} className="button"/>
         <input type="button" value="7" onClick={clickHandler} className="button"/>
         <input type="button" value="6" onClick={clickHandler} className="button"/>
         <input type="button" value="5" onClick={clickHandler} className="button"/>
         <input type="button" value="4" onClick={clickHandler} className="button"/>
         <input type="button" value="3" onClick={clickHandler} className="button"/>
         <input type="button" value="2" onClick={clickHandler} className="button"/>
         <input type="button" value="1" onClick={clickHandler} className="button"/>
         <input type="button" value="0" onClick={clickHandler} className="button"/>
         <input type="button" value="." onClick={clickHandler} className="button"/>
         <input type="button" value="/" onClick={clickHandler} className="button"/>
         <input type="button" value="*" onClick={clickHandler} className="button"/>
         <input type="button" value="+" onClick={clickHandler} className="button"/>
         <input type="button" value="-" onClick={clickHandler} className="button"/>
         <input type="button" value="clear" onClick={clearDisplay} className="button-special"/>
         <input type="button" value="=" onClick={calculate} className="button-special"/>
       </form>
       </div>
      </div>
    );
}



export default App;
